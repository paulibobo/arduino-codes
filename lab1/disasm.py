Import("env", "projenv")

env.AddPostAction(
    "$BUILD_DIR/${PROGNAME}.elf",
    env.VerboseAction(" ".join([
        "avr-objdump", "-S",
        "$BUILD_DIR/${PROGNAME}.elf", ">", "${PROGNAME}.txt"
    ]), "Disassembly listing generated for $BUILD_DIR/${PROGNAME}.hex")
)